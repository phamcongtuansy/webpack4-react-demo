import React from 'react'
import style from './user-table.scss'

const UserTable = props => (
  <table className={style.usertable}>
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.users.length > 0 ? (
        props.users.map(user => (
          <tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
            <td>
              <button
                onClick={() => {
                  props.editRow(user)
                }}
              >
                Edit
              </button>
              <button
                onClick={() => props.deleteUser(user.id)}
              >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
          <tr>
            <td colSpan={4}>No users</td>
          </tr>
        )}
    </tbody>
  </table>
)

export default UserTable
