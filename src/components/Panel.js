import React, { useState, useEffect } from 'react'

const Panel = (props) => {
    return <div>{props.children}</div>
}

export default Panel;
