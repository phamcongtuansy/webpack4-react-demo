import React, { useState, useEffect } from 'react'
import style from './Tab.scss'

const Tab = (props) => {
    const [selected, setSelected] = useState(props.selected || 0)

    return (
        <div>
            <ul className={style.inline}>
                {props.children.map((elem, index) => {
                    return <li style={{ color: index == selected ? `#337ab7` : 'unset' }} className={style.active} key={index} onClick={() => setSelected(index)}>{elem.props.title}</li>
                })}
            </ul>
            <div className={style.tab}>{props.children[selected]}</div>
        </div>
    )
}

export default Tab;