import React, { useState } from 'react'
import UserForm from './forms/UserForm'
import UserTable from './tables/UserTable'
import { useLocalStorage } from './hooks'
import style from './App.scss'
const App = () => {
  // Data
  const usersData = [
    { id: 1, name: 'Apple', email: 'apple@apple.inc', phone: 123123123 },
    { id: 2, name: 'Samsung', email: 'samsung@ss.kr', phone: 123123123 },
    { id: 3, name: 'Nokia', email: 'nokia@legend.com', phone: 123123123 },
  ]

  const initialFormState = { id: null, name: '', email: '', phone: '' }

  // Setting state
  const [users, setUsers] = useLocalStorage('usersData', usersData)

  const [currentUser, setCurrentUser] = useState(initialFormState)
  const [editing, setEditing] = useState(false)
  const [showModal, setShowModal] = useState(false)

  // CRUD operations
  const addUser = user => {
    user.id = users.length + 1
    setUsers([...users, user])
  }

  const deleteUser = id => {
    setEditing(false)

    setUsers(users.filter(user => user.id !== id))
  }

  const updateUser = (id, updatedUser) => {
    setEditing(false)

    setUsers(users.map(user => (user.id === id ? updatedUser : user)))
  }

  const editRow = user => {
    setEditing(true)
    setCurrentUser({ id: user.id, name: user.name, email: user.email, phone: user.phone })
    setShowModal(true)
  }

  const toggleModal = () => {
    setShowModal(!showModal)
    setEditing(false)
  }

  return (
    <div className={style.app}>
      <div className="flex-row">
        <h1>Webpack4 + React</h1>
        <UserForm editing={editing} currentUser={currentUser} toggleModal={toggleModal} showModal={showModal} addUser={addUser} updateUser={updateUser} />
        <div className="flex-large">
          <h2>View users</h2>
          <UserTable users={users} editRow={editRow} deleteUser={deleteUser} />
          <button onClick={toggleModal}>Add new user</button>
        </div>
      </div>
    </div>
  )
}

export default App
