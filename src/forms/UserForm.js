import React, { useState, useEffect } from 'react'
import { Tab, Panel } from '../components'
import style from './form.scss'

const UserForm = props => {
	const { showModal, toggleModal, addUser, currentUser, editing, updateUser } = props
	const initialFormState = editing ? { ...currentUser } : { id: null, name: '', email: '', phone: '' }
	const [user, setUser] = useState(initialFormState)

	const handleInputChange = event => {
		const { name, value } = event.target
		setUser({ ...user, [name]: value })
	}

	useEffect(() => {
		setUser(initialFormState)
	}, [editing])

	const onAddUser = () => {
		if (!user.name || !user.email || !user.phone) return

		addUser(user)
		setUser(initialFormState)
		toggleModal()
	}

	const onEditUser = () => {
		if (!user.name || !user.email || !user.phone) return

		updateUser(user.id, user)
		setUser(initialFormState)
		toggleModal()
	}

	return (
		<div
			style={{ display: showModal ? 'block' : 'none' }}
			className={style.ctn}
		>
			<div className={style.form} >
				<Tab selected={0} >
					<Panel title="Name">
						<input placeholder="Name" type="text" name="name" value={user.name} onChange={handleInputChange} />
					</Panel>
					<Panel title="Email">
						<input placeholder="Email" type="text" name="email" value={user.email} onChange={handleInputChange} />
					</Panel>
					<Panel title="Phone">
						<input placeholder="Phone" type="text" name="phone" value={user.phone} onChange={handleInputChange} />
					</Panel>
				</Tab>
				{
					editing ?
						<button type="button" onClick={onEditUser}>Save</button>
						:
						<button type="button" onClick={onAddUser}>Add</button>
				}
				<button type="button" onClick={toggleModal} className={style.cancel}>Cancel</button>
			</div>
		</div>
	)
}

export default UserForm
